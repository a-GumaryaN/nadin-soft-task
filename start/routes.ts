import Route from "@ioc:Adonis/Core/Route";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import User from "App/Models/User";

Route.group(() => {
  /**
   *
   * all groups that should be authenticated
   *
   */
}).middleware(["auth"]);

/**
 *
 * route for generate JWT token
 *
 */
Route.post("/login", async ({ auth }: HttpContextContract) => {
  /**
   * check existing user in system
   */
  const user = await User.find(1);

  /**
   * return error if user found
   */

  if (!user) return { error: "No user found" };

  /**
   * generate and return token to client
   */

  return await auth.use("jwt").generate(user);
});

/**
 *
 * route for refreshing JWT token
 *
 */
Route.post("/refresh", async ({ auth, request }: HttpContextContract) => {
  const refreshToken = request.input("refresh_token");

  return await auth.use("jwt").loginViaRefreshToken(refreshToken);
});

/**
 * 
 * route for logout
 * 
 */
Route.post("/logout", async ({ auth }: HttpContextContract) => {
  await auth.use("jwt").revoke();
  return {
    revoked: true,
  };
});


/**
 *
 * 404 route
 *
 */
Route.get("", async ({ response }) => {
  response.status(404).send({ error: "404 , route not found" });
});
